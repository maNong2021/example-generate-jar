概述：

通过gradle的application插件进行打包。此方法配置简单，因为依赖包和应用jar包分开解决了引入多个spring相关jar的时候文件冲突的问题，此外bin目录下还生成了可一键执行的.bat程序。



配置步骤

1. 在build.gradle配置文件中添加如下配置

   ```
   version '0.0.3-SNAPSHOP'
   def appMainClass = 'com.example.generate.HelloWorld'
   apply plugin: 'application'
   mainClassName = appMainClass
   ```

   

2. 打开项目gradlew 文件所在目录，然后执行gradlew build 命令即可在build/distributions目录中查看到生成的jar包和zip包，压缩包里面的lib目录即包括了应用程序包和依赖包，压缩包里面的bin目录即可看到应用的执行文件。如下：

```
E:\PcGroupGit\example-generate-jar>gradlew build

Deprecated Gradle features were used in this build, making it incompatible with Gradle 7.0.
Use '--warning-mode all' to show the individual deprecation warnings.
See https://docs.gradle.org/6.7/userguide/command_line_interface.html#sec:command_line_warnings

BUILD SUCCESSFUL in 1s

```

