概述：通过gradle打可执行jar包，并且将依赖一起解压到jar包中

步骤：
1. 在gradle配置中添加如下配置

   ```
   jar {
       archivesBaseName = 'gradle-generate-jar-include-dependence'//基本的文件名
       archiveVersion = '0.0.1' //版本
       manifest { //配置jar文件的manifest
           attributes(
                   "Manifest-Version": 1.0,
                   'Main-Class': 'com.example.generate.HelloWorld' //指定main方法所在的文件
           )
       }
       //打包依赖包
       from {
           (configurations.runtimeClasspath).collect {
               it.isDirectory() ? it : zipTree(it)
           }
       }
   }
   ```

2. 打开项目gradlew 文件所在目录，然后执行gradlew build 命令即可在build/libs目录中查看到生成的jar。如下：

   ```
   E:\PcGroupGit\example-generate-jar>gradlew build
   
   Deprecated Gradle features were used in this build, making it incompatible with Gradle 7.0.
   Use '--warning-mode all' to show the individual deprecation warnings.
   See https://docs.gradle.org/6.7/userguide/command_line_interface.html#sec:command_line_warnings
   
   BUILD SUCCESSFUL in 1s
   2 actionable tasks: 2 up-to-date
   E:\PcGroupGit\example-generate-jar>
   ```

   

缺点：

1.  由于将所有的依赖jar都解压到打包的jar中，如果是spring项目可能会引起文件冲突导致打包失败
2.  生成的jar包巨大，影响部署效率
3.  无法单独引用某个依赖包