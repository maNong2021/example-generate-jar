概述：
通过gradle打可运行jar包，并且将依赖的jar放到可运行jar包之外的指定目录中。



操作步骤：

1. 在build.gradle配置文件中添加如下配置

   ```
   jar {
       archivesBaseName = 'gradle-generate-jar-no-include-dependence'//基本的文件名
       archiveVersion = '0.0.2' //版本
       manifest { //配置jar文件的manifest
           attributes(
                   "Manifest-Version": 1.0,
                   'Main-Class': 'com.example.generate.HelloWorld' //指定main方法所在的文件
           )
       }
   }
   
   task clearJar(type: Delete) {
       delete 'build/libs/lib'
   }
   
   task copyJar(type: Copy) {
       from configurations.runtime
       into('build/libs/lib')
   }
   
   task release(type: Copy, dependsOn: [build, clearJar, copyJar])
   ```

   

2. 打开项目gradlew 文件所在目录，然后执行gradlew release 命令即可在build/libs目录中查看到生成的jar。如下：

   ```
   E:\PcGroupGit\example-generate-jar>gradlew release
   
   Deprecated Gradle features were used in this build, making it incompatible with Gradle 7.0.
   Use '--warning-mode all' to show the individual deprecation warnings.
   See https://docs.gradle.org/6.7/userguide/command_line_interface.html#sec:command_line_warnings
   
   BUILD SUCCESSFUL in 1s
   4 actionable tasks: 3 executed, 1 up-to-date
   E:\PcGroupGit\example-generate-jar>
   
   ```

   

   3. 通过如下命令运行程序：

   ```
   方式1：
   java -Xbootclasspath/a:lib/*  -jar gradle-generate-jar-no-include-dependence-0.0.2.jar

   方式2：
java -cp ./lib/log4j-1.2.17.jar -jar gradle-generate-jar-no-include-dependence-0.0.2.jar
   
   说明：方式2的需要把依赖包全部显示列出来，不支持通配符。这种方式缺点比较明显，对大量依赖包的非常不友好
   ```

   **注：**
   
   **-Xbootclasspath:完全取代系统Java classpath.最好不用。**
   **-Xbootclasspath/a: 在系统class加载后加载。一般用这个。**
   **-Xbootclasspath/p: 在系统class加载前加载,注意使用，和系统类冲突就不好了.**
   
   